package apoprojet;
public class Alternatif extends Scrutin{

	public Candidat gagnant() {
		BulAlt votes[] = new BulAlt[nbelecteurs];
		for(int i=0; i<nbelecteurs; i++) {
			votes[i] = new BulAlt();
		}
		int classmt;
		int min = nbelecteurs +1;
		int tempindice =0;
		for (int i=0;i<nbcandidats;i++){
            listeCandidat[i].initCandidat();
        }
		for (int i=0; i<nbelecteurs; i++) {
			votes[i].trier(this.tabElecteurs[i], this.listeCandidat);
			classmt = votes[i].getClassement(0);
			listeCandidat[classmt].setNbrVoix(listeCandidat[classmt].getNbrVoix() +1);
		}
		for(int j = 0; j<nbcandidats-1;j++) {
			for (int i=0;i<nbcandidats;i++){
	            if(listeCandidat[i].getNbrVoix()<min && listeCandidat[i].getNbrVoix()>-1){
	                min = listeCandidat[i].getNbrVoix();
	                tempindice = i;
	            }
	        }
			listeCandidat[tempindice].setNbrVoix(-1);
			for(int i=0; i<nbelecteurs; i++) {
				if(votes[i].getClassement(0) == tempindice) {
					classmt = votes[i].getClassement(1);
					if(listeCandidat[classmt].getNbrVoix() > -1) {
						listeCandidat[classmt].setNbrVoix(listeCandidat[classmt].getNbrVoix() +1);
					}
				}
			}
		}
		for (int i=0;i<nbcandidats;i++){
            if(listeCandidat[i].getNbrVoix()>0){
                tempindice = i;
            }
        }
        System.out.println("Le candidat " + tempindice + " a gagné l'élection alternative" );
        return listeCandidat[tempindice];
	}

	
}
