package apoprojet;

public class BulAlt extends Alternatif{
	
	private double eletaux[] = new double[nbcandidats];
	private int classement[] = new int[nbcandidats];
	
	public int getClassement(int indice){
		return classement[indice];
	}
	
	public void trier(Electeurs e, Candidat[] listeCandidat) {
		double max =-1;
		int dejavu=0;
		int tempindice=0;
		for(int j=0; j<nbcandidats;j++) {
			eletaux[j] = tauxPreference(e, listeCandidat[j]);
		}
		for(int j=0; j<nbcandidats; j++) {
			max = -1;
			if(j == 0) {
				for(int k=0; k<nbcandidats; k++) {
					if(eletaux[k]>max){
						max = eletaux[k];
						tempindice = k;
					}
				}
				classement[tempindice] = j;
				dejavu = tempindice;
			}
			else {
				for(int k=0; k<nbcandidats; k++) {
					if(eletaux[k]>max && eletaux[k] < eletaux[dejavu]) {
						max = eletaux[k];
						tempindice = k;
					}
				}
				classement[tempindice]=j;
				dejavu = tempindice;
			}
		}
		
	}
	
	public double tauxPreference(Electeurs e, Candidat c){
        double checkaxe = 0;
        for (int i=0;i<e.nbaxes;i++){
            checkaxe = checkaxe + Math.abs(e.getAxesAIndice(i) - c.getAxesAIndice(i));
        }
        checkaxe = checkaxe - e.nbaxes*0.1;
        return checkaxe;
    }
}
