package apoprojet;

public class Sondage extends Scrutin{
    
    public void faireSondage (int n){
        for (int i=0;i<nbcandidats;i++){
            listeCandidat[i].initCandidat();
        }
        voteblanc.setNbrVoix(0);
        double perc = 0;
        for (int i=0;i<n;i++){
            preference(this.tabElecteurs[i]);
        }
        for(int j=0;j<nbcandidats;j++){
            perc = (double)((listeCandidat[j].getNbrVoix()*100)/n);
            System.out.println("Le candidat " + j + " a obtenu " + perc + "% des voix sur un sondage de " + n + " personnes.");
        }
        perc = (double)(voteblanc.getNbrVoix()*100)/n;
        System.out.println(perc + "% des individus interrogés n'ont pas d'avis.");
        faireEvoluer();
    }

    public void faireEvoluer(){
        double result = 0;
        for (int j=0;j<nbcandidats;j++){
            for(int i=0;i<nbelecteurs;i++){
                for (int k=0;k<tabElecteurs[0].nbaxes;k++){
                    if(Math.abs(Math.sqrt(Math.pow(tabElecteurs[i].getAxesAIndice(k) - listeCandidat[j].getAxesAIndice(k), 2))) <= (double)1/4){
                        if (tabElecteurs[i].getAxesAIndice(k) - listeCandidat[j].getAxesAIndice(k) >= 0){
                            result = tabElecteurs[i].getAxesAIndice(k) - (Math.abs(Math.sqrt(Math.pow(tabElecteurs[i].getAxesAIndice(k) - listeCandidat[j].getAxesAIndice(k),2))));
                            tabElecteurs[i].setAxes(k,result);
                        }
                        if (tabElecteurs[i].getAxesAIndice(k) - listeCandidat[j].getAxesAIndice(k) >= 0){
                            result = tabElecteurs[i].getAxesAIndice(k) + (Math.abs(Math.sqrt(Math.pow(tabElecteurs[i].getAxesAIndice(k) - listeCandidat[j].getAxesAIndice(k),2))));
                            tabElecteurs[i].setAxes(k, result);
                        }
                    }
                }
            }
        }
    }
    
}
