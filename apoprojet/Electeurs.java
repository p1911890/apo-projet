package apoprojet;
import java.util.Random;

public class Electeurs {
    protected int nbaxes=3;
    protected double axe[] = new double[nbaxes];

    public Electeurs(double p, double e, double edu){
        axe[0]=p;
        axe[1]=e;
        axe[2]=edu;
    }

    public Electeurs(){
        for (int i=0;i<nbaxes;i++){
            Random random = new Random();
            double nb;
            nb = random.nextInt(11);
            nb = nb/10;
            axe[i] = nb;
        }
    }

    public void affElecteurs(){
        for (int i=0;i<nbaxes;i++){
            System.out.println(axe[i]);
        }
    }

    public void setAxes(int k, double val){
        this.axe[k] = val;
    }

    public int getAxes(){
        return nbaxes;
    }

    public double getAxesAIndice(int i){
        if (i<nbaxes && i>=0){
            return axe[i];
        }else
        return 0;        
    }

    public double getSommeAxes(){
        double somme=0;
        for (int i=0;i<nbaxes;i++){
            somme= somme + this.axe[i];
        }
        return somme;
    }
}
