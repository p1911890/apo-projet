package apoprojet;

public class MajoritaireDeuxTours extends Scrutin{
    public Candidat gagnant(){
        int max1 = 0;
        int max2 = 0;
        int tempindice=0;
        int tempindice2=0;
        double perc1 = 0;
        double perc2 = 0;
        double perc = 0;
        for (int i=0;i<nbcandidats;i++){
            listeCandidat[i].initCandidat();
        }
        for(int i=0;i<nbelecteurs;i++){
            preference(this.tabElecteurs[i]);
        }
        for (int i=0;i<nbcandidats;i++){
            if(listeCandidat[i].getNbrVoix()>max1){
                max1 = listeCandidat[i].getNbrVoix();
                tempindice = i;
            }
            if(listeCandidat[i].getNbrVoix()>max2 && listeCandidat[i].getNbrVoix()<max1){
                max2 = listeCandidat[i].getNbrVoix();
                tempindice2 = i;
            }
        }
        perc1 = (double)(listeCandidat[tempindice].getNbrVoix()*100)/nbelecteurs;
        perc2 = (double)(listeCandidat[tempindice2].getNbrVoix()*100)/nbelecteurs;
        System.out.println("Les deux candidats passants au second tours sont " + tempindice + " et " + tempindice2 + " avec respectivement " + perc1 + " et " + perc2 + " pourcents.");
        listeCandidat[tempindice].setNbrVoix(0);
        listeCandidat[tempindice2].setNbrVoix(0);
        for (int i=0;i<nbelecteurs;i++){
            if(tauxPreference(this.tabElecteurs[i], listeCandidat[tempindice]) >= tauxPreference(this.tabElecteurs[i], listeCandidat[tempindice2]) &&
            tauxPreference(this.tabElecteurs[i],listeCandidat[tempindice])<=1)
            {
                listeCandidat[tempindice].setNbrVoix(listeCandidat[tempindice].getNbrVoix() + 1);
            }
            else if(tauxPreference(this.tabElecteurs[i], listeCandidat[tempindice]) <= tauxPreference(this.tabElecteurs[i], listeCandidat[tempindice2]) &&
            tauxPreference(this.tabElecteurs[i],listeCandidat[tempindice2])<=1)
            {
                listeCandidat[tempindice2].setNbrVoix(listeCandidat[tempindice2].getNbrVoix() + 1);
            }
        }
        if (listeCandidat[tempindice].getNbrVoix() >= listeCandidat[tempindice2].getNbrVoix()){
            perc = (double)(listeCandidat[tempindice].getNbrVoix()*100)/nbelecteurs;
            System.out.println("Le gagnant de l'élection est " + tempindice + " avec " + perc + " pourcents.");
            return listeCandidat[tempindice];
        }
        else{
            perc = (double)(listeCandidat[tempindice2].getNbrVoix()*100)/nbelecteurs;
            System.out.println("Le gagnant de l'élection est " + tempindice2 + " avec " + perc + " pourcents.");
            return listeCandidat[tempindice2];
        }
    }

    public double tauxPreference (Electeurs e, Candidat c){
        double checkaxe = 0;
        for (int i=0;i<e.nbaxes;i++){
            checkaxe = checkaxe + Math.abs(e.getAxesAIndice(i) - c.getAxesAIndice(i));
        }
        checkaxe = checkaxe - e.nbaxes*0.1;
        return checkaxe;
    }
}
