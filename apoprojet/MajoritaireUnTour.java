package apoprojet;

public class MajoritaireUnTour extends Scrutin {

    public Candidat gagnant(){
        int max = 0;
        int tempindice=0;
        double perc = 0;
        for (int i=0;i<nbcandidats;i++){
            listeCandidat[i].initCandidat();
        }
        for(int i=0;i<nbelecteurs;i++){
            preference(this.tabElecteurs[i]);
        }
        for (int i=0;i<nbcandidats;i++){
            if(this.listeCandidat[i].getNbrVoix()>max){
                max = this.listeCandidat[i].getNbrVoix();
                tempindice = i;
            }
        }
        perc = (double)(this.listeCandidat[tempindice].getNbrVoix()*100)/nbelecteurs;
        System.out.println("Le candidat " + tempindice + " a gagné l'élection majoritaire à un tour avec " + perc + "% des voix." );
        return this.listeCandidat[tempindice];
    }
}
