package apoprojet;

public class Candidat extends Electeurs {
    private String nom;
    private String prenom;
    private int nombreVoix;

    public void setNbrVoix(int nb){
        this.nombreVoix = nb;
    }

    public int getNbrVoix(){
        return this.nombreVoix;
    }

    public void initCandidat(){
        this.setNbrVoix(0);
    }
}
