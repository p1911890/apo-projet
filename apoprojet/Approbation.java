package apoprojet;
public class Approbation extends Scrutin{

	public Candidat gagnant(){
        int max = -1;
        double tolerance=1;
        int tempindice =0;
        double perc = 0;
        for (int i=0;i<nbcandidats;i++){
            listeCandidat[i].initCandidat();
        }
        for(int i=0;i<nbelecteurs;i++){
        	for(int j=0; j<nbcandidats; j++) {
                if(tauxPreference(this.tabElecteurs[i], this.listeCandidat[j])<tolerance) {
                	listeCandidat[j].setNbrVoix(listeCandidat[j].getNbrVoix()+1);
                }
        	}
        }
        for (int i=0;i<nbcandidats;i++){
            if(listeCandidat[i].getNbrVoix()>max){
                max = listeCandidat[i].getNbrVoix();
                tempindice = i;
            }
        }
        perc = (double)(listeCandidat[tempindice].getNbrVoix()*100)/nbelecteurs;
        System.out.println("Le candidat " + tempindice + " a gagné l'élection par approbation avec " + perc + "% d'approbation." );
        return listeCandidat[tempindice];
    }
	
	public double tauxPreference (Electeurs e, Candidat c){
        double checkaxe = 0;
        for (int i=0;i<e.nbaxes;i++){
            checkaxe = checkaxe + Math.abs(e.getAxesAIndice(i) - c.getAxesAIndice(i));
        }
        checkaxe = checkaxe - e.nbaxes*0.1;
        return checkaxe;
    }
}
