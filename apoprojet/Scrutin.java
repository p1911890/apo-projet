package apoprojet;

public class Scrutin {

    protected int nbcandidats = 10;
    protected Candidat listeCandidat[] = new Candidat[nbcandidats];
    protected Candidat voteblanc = new Candidat();
    protected int nbelecteurs = 1000;
    protected Electeurs tabElecteurs[] = new Electeurs[nbelecteurs];

    public Candidat preference(Electeurs e){
        int nbaxes = e.getAxes();
        int tempindice=0;
        double checkaxe[] = new double[nbaxes];
        double tempaxe = nbaxes*3;
        double somme = 0;

        for (int j = 0; j<nbcandidats;j++){
        	for (int i=0; i<nbaxes;i++){
        		checkaxe[i] = Math.abs(Math.sqrt(Math.pow(e.getAxesAIndice(i) - listeCandidat[j].getAxesAIndice(i), 2)));
        		somme = somme + checkaxe[i];
        	}
        	if (somme <= tempaxe){
        		tempaxe = somme;
        		tempindice = j;
        	}
        	for (int i = 0; i<nbaxes;i++){
        		checkaxe[i] = 0;
        		somme = 0;
        	}
        }
        if(tempaxe < 1){
        	listeCandidat[tempindice].setNbrVoix(listeCandidat[tempindice].getNbrVoix()+1);
        	System.out.println("Cet électeur vote pour le candidat " + tempindice);
        	return listeCandidat[tempindice];
        }
        else{
            voteblanc.setNbrVoix(voteblanc.getNbrVoix()+1);
            return voteblanc;
        }
    }
    
    public void setCandidat(int nb){
        nbcandidats = nb;
        for (int i=0; i<nbcandidats;i++){
            Candidat c = new Candidat();
            listeCandidat[i] = c;
        }
    }

    public Candidat[] getCandidat(){
        return listeCandidat;
    }


    public void setElecteur(int nb){
        nbelecteurs = nb;
        for(int i=0;i<nbelecteurs;i++){
            Electeurs e = new Electeurs();
            tabElecteurs[i] = e;
        }
    }

    public void setTabElecteur(Electeurs [] tab, int nbelecteurs){
        this.nbelecteurs = nbelecteurs;
        for(int i=0;i<nbelecteurs;i++){
            this.tabElecteurs[i] = tab[i];
        }
    }

    public void setListeCandidat(Candidat [] tab, int nbcandidats){
        this.nbcandidats = nbcandidats;
        for(int i=0;i<nbcandidats;i++){
            this.listeCandidat[i] = tab[i];
        }
    }
}
