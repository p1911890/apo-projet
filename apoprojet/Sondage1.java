package apoprojet;

public class Sondage1 extends Sondage{
	
	public void sonderPop(int npremiers, int nbparticipants) {
		this.faireSondage(nbparticipants);
		int classement[] = new int[npremiers];
		int tempindice = 0;
		int dejavu=0;
		for(int i=1; i<nbcandidats; i++) {
			if(this.listeCandidat[i].getNbrVoix()> this.listeCandidat[tempindice].getNbrVoix()) {
				tempindice = i;
			}
		}
		dejavu = tempindice;
		classement[0] = tempindice;
		for(int j=1; j<npremiers; j++) {
			tempindice = 0;
			for(int i=1; i<nbcandidats; i++) {
				if(this.listeCandidat[i].getNbrVoix()> this.listeCandidat[tempindice].getNbrVoix() 
						&& this.listeCandidat[i].getNbrVoix()< this.listeCandidat[dejavu].getNbrVoix() ) {
					tempindice = i;
				}
			}
			dejavu = tempindice;
			classement[j] = tempindice;
		}
		for(int i =0; i<npremiers; i++) {
			double perc = (double)((listeCandidat[classement[i]].getNbrVoix()*100)/nbparticipants);
	        System.out.println("Le candidat " + classement[i] + " a obtenu " + perc + "% des voix sur un sondage de " + nbparticipants + " personnes.");
		}
	    for(int i=0; i<nbparticipants; i++) {
			int max =0 ;
			for(int j=1; j<npremiers; i++) {
				if(tauxPreference(this.tabElecteurs[i], this.listeCandidat[j]) > tauxPreference(this.tabElecteurs[i], this.listeCandidat[max])) {
					max = j;
				}
			}
			
			faireEvoluerElecteur(this.tabElecteurs[i], this.listeCandidat[max]);
		}
	}

	public double tauxPreference(Electeurs e, Candidat c){
        double checkaxe = 0;
        for (int i=0;i<e.nbaxes;i++){
            checkaxe = checkaxe + Math.abs(e.getAxesAIndice(i) - c.getAxesAIndice(i));
        }
        checkaxe = checkaxe - e.nbaxes*0.1;
        return checkaxe;
    }
	
	public void faireEvoluerElecteur(Electeurs e, Candidat c){
		double newaxe;
		for (int i=0;i<e.nbaxes;i++){
            newaxe = e.getAxesAIndice(i)*0.5 + c.getAxesAIndice(i)*0.5;
            e.setAxes(i, newaxe);
        }
	}
}
