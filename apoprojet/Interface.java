package apoprojet;

public class Interface {

	public void IMH(){

		java.util.Scanner input = new java.util.Scanner(System.in);
		Scrutin s = new Scrutin();
		System.out.println("Combiend d'électeurs voulez-vous (maximum 1000)");
		int nbelecteurs = input.nextInt();
		s.setElecteur(nbelecteurs);
		System.out.println("Combien de candidats voulez-vous(maximum 1000)");
		int nbcandidats = input.nextInt();
		s.setCandidat(nbcandidats);
		boolean continuer = true;
		while(continuer == true){
			System.out.println("Que voulez vous faire ?");
			System.out.println("1. Sondage");
			System.out.println("2. Scrutin");
			System.out.println("3. Quitter");
			System.out.println("Tapez le numero correspondant a votre choix");
			switch (input.nextInt()){
			case 1:
				Sondage1 p = new Sondage1();
				p.setTabElecteur(s.tabElecteurs,nbelecteurs);
				p.setListeCandidat(s.listeCandidat,nbcandidats);
				System.out.println("Le sondage sera r�alis� sur le premier mod�le de sondag. Quelle est la limite n que vous voulez fixer?");
				int nbsondage = input.nextInt();
				if(nbsondage<=nbcandidats){
					System.out.println("combien de personnes participent � ce sondage?");
					int nbparticipants = input.nextInt();
					if(nbparticipants <= nbelecteurs) {
						p.sonderPop(nbsondage, nbparticipants);
						System.out.println("Les votes ont évolué");
					}
					else {
						System.out.println("le nombre d'electeurs est plus petit que le nombre de participants");
					}
				}
				else{
					System.out.println("Le nombre limite de candidats du sondage est supérieur au nombre d'électeur total");
				}
				break;
			case 2 :
				System.out.println("Quel mode de scrutin voulez vous ?");
				System.out.println("1. Scrutin majoritaire à un tour");
				System.out.println("2. Scrutin majoritaire à deux tours");
				System.out.println("3. Vote par approbation");
				System.out.println("4. Vote alternatif");
				System.out.println("5. M�thode de Borda");
				int choixsondage =input.nextInt();
				switch (choixsondage){
				case 1 :
					MajoritaireUnTour m1 = new MajoritaireUnTour();
					m1.setCandidat(nbcandidats);
					m1.setElecteur(nbelecteurs);
					m1.gagnant();
					break;
				case 2 :
					MajoritaireDeuxTours m2 = new MajoritaireDeuxTours();
					m2.setCandidat(nbcandidats);
					m2.setElecteur(nbelecteurs);
					m2.gagnant();
					break;
				case 3:
					Approbation appro = new Approbation();
					appro.setCandidat(nbcandidats);
					appro.setElecteur(nbelecteurs);
					appro.gagnant();
					break;
				case 4:
					Alternatif alt = new Alternatif();
					alt.setCandidat(nbcandidats);
					alt.setElecteur(nbelecteurs);
					alt.gagnant();
					break;
				case 5:
					Borda bord = new Borda();
					bord.setCandidat(nbcandidats);
					bord.setElecteur(nbelecteurs);
					bord.gagnant();
				default : 
					break;
				}
			default :
				continuer=false;
				break;
			}
		}
	}
}
