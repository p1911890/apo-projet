package apoprojet;

public class Borda extends Scrutin{
	
	public Candidat gagnant() {
		BulAlt votes[] = new BulAlt[nbelecteurs];
		for(int i=0; i<nbelecteurs; i++) {
			votes[i] = new BulAlt();
		}
		int classmt;
		int max = -1;
		int tempindice =0;
		for (int i=0;i<nbcandidats;i++){
            listeCandidat[i].initCandidat();
        }
		for (int i=0; i<nbelecteurs; i++) {
			votes[i].trier(this.tabElecteurs[i], this.listeCandidat);
			for(int j=0; j<nbcandidats; j++) {
				classmt = votes[i].getClassement(j);
				listeCandidat[classmt].setNbrVoix(listeCandidat[classmt].getNbrVoix() +nbcandidats-j);
			}
		}
		
		for (int i=0;i<nbcandidats;i++){
            if(listeCandidat[i].getNbrVoix()>max){
                max = listeCandidat[i].getNbrVoix();
                tempindice = i;
            }
        }
        int points = listeCandidat[tempindice].getNbrVoix();
        System.out.println("Le candidat " + tempindice + " a gagné l'élection par syst�me de Borda avec " + points + "points." );
        return listeCandidat[tempindice];
		
	}
}
